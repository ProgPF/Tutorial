# Einführung in die Programmierung

Begleitend zum Modul 8822 "Einführung in die Programmierung" werden
in diesem Projekt Erläuterung und Anleitungen als Markdown-Dokumente
bereitgestellt. Um die Präsentationsansicht zu starten, klicken Sie
bitte oben rechts auf *Open Preview* (Fenstersymbol mit Lupe) oder
verwenden Sie den Shortcut `CTRL+K V`.

> ## 📖 Lernziele
>
> - Python als moderne Programmiersprache erlernen
> - Grundkonzepte von Programmiersprachen kennen
>   - Datentypen
>   - Schleifen und Entscheidungen
>   - Algorithmen und Datenstrukturen
> - Einführung objektorientiertes Programmieren
> - Erste Kenntnisse in Data-Learning-Bibliotheken
> - Sourcecode organisieren können
> - Techniken zur Fehlersuche erlernen
>   - Tracing und Debugging

## Vorlesungsinhalte

### Unit 1: Erste Schritte

- [Konzept der Programmiersprachen](01_Intro/lesson/ProgrammingLanguage.md)
- [Installation der Entwicklungsumgebung](01_Intro/lesson/Setup.md)
- [Visual Studio Code: Benutzerinterface](01_Intro/lesson/MainWindow.md)
- [The ever returning "Hello World"](01_Intro/lesson/HelloWorld.md)
- [Konzepte des Minimalprogramms](01_Intro/lesson/ExtHelloWorld.md)
