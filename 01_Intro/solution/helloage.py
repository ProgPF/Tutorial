# Aufgabe: Ein-/und Ausgabe mit Erfassung des Alters
# Nebenaufgabe: Debugger testen

# Step 1: Name und Alter in der Konsole eingeben und in einer Variable speichern
your_name = input("Please enter your name: ")
age = int(input("How old are you? "))

# Step 2: Aus Alter und dem aktuellen Jahr ihr Geburtsjahr ausrechnen
#         Zunächst das so behandeln, als seien Sie am 1.1. des Jahres geboren
birth_year = 2023 - age

# Step 3 : Alle Daten in Form eines f-Strings ausgeben
print(f"Welcome user {your_name}. Your current age is {age}.")
print(f"You were born in {birth_year}")
