# Helloworld mit persönlicher Begrüßung

# Step 1: Namen in der Konsole eingeben und in einer Variable speichern
your_name = input("Please enter your name: ")

# Step 2: Namen mit Hallo-Nachricht verknüpfen und in der Konsole ausgeben
print("Hello my dear " + your_name)
