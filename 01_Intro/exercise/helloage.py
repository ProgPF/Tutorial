# Aufgabe: Ein-/und Ausgabe mit Erfassung des Alters
# Nebenaufgabe: Debugger testen

# Step 1: Name und Alter in der Konsole eingeben und in einer Variable speichern

# Step 2: Aus Alter und dem aktuellen Jahr ihr Geburtsjahr ausrechnen
#         Zunächst das so behandeln, als seien Sie am 1.1. des Jahres geboren

# Step 3: Alle Daten in Form eines f-Strings ausgeben
