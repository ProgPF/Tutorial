# Visual Studio: User Interface

## Grundelemente

Das Hauptfenster von Visual Studio besteht aus 5 Elementen:

- *Activity Bar:* Toolbar an der linken Seite, Zugriff auf aufgabenbezogene Ansichten innerhalb der Sidebar.
- *Side Bar (B):* Die Sidebar stellt Werkzeuge und weitere Informationen zum aktuellen Projekt / zur aktuellen Datei bereit. Sofern aktiviert, gibt es linke und rechte Sidebar.
- *Editor:* Der eigentliche Editierbereich. Alle offenen Sourcecode-Dateien werden über Tabs getrennt. Kann vertikal und horizontal gesplittet werden.
- *Panel:* Ausgabebereich mit mehreren Tabs. Im Abschnitt *Terminal* können Sie direkt mit dem Betriebssystem interagieren.
- *Status Bar:* Statusinformationen zum Projekt / zur aktuellen Datei

![Ansicht Hauptfenster](../../_assets/vsc_ux.png)

## Command Palette

Alle Funktionen innerhalb von Visual Studio Code können über die Tastatur erreicht werden. Dies ist im Entwicklungsalltag effizienter, als die Bedienung über die Maus. Der Zugriff auf die Funktionen erfolgt gebündelt über die **Command Palette**.

Zum Öffnen die Tastenkombination `CTRL+SHIFT+P` verwenden:

![Command Palette](../../_assets/01_vsc_ux_cmdpalette.png)

## Zugriff über die Suchfunktion

Alternativ kann die Suchfunktion von Visual Studio dazu genutzt werden, Kommandos zu finden. Zunächst die Suchleiste anklicken (unter Window am oberen Bildschirmrand) oder über den Shortcut `CTRL+P` öffnen. Im Suchfenster kann jetzt entweder ein Dateiname oder ein Kommandozeichen eingegeben werden:

- **>** : Greift auf die Funktionen von VS Code zu. Beispiel: `> for` um *Format Document* zu erreichen
- **@** : Zeigt die Symbole innerhalb der aktuellen Sourcecodedatei an.
Erlaubt es, direkt zur Definition von Funktionen und Variablen in der aktuellen Datei zu springen.
- **#** : Zeigt die Symbole innerhalb des Projektes an. Erlaubt es, direkt die Definition von Funktionen und Variablen im aktuellen Projekt zu öffnen.
- **:** : Springe zu einer Zeilennummer innerhalb der aktuellen Datei

![Verwendung der Suchleiste](../../_assets/01_vsc_ux_search_symbol.png)


[Weiter](HelloWorld.md)