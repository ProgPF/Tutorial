# Hello World / Teil 2

Bislang ist unser erstes **Hello World**-Programm nicht interaktiv. Typischerweise sollen Programme Daten oder Eingaben entgegennehmen, etwas berechnen und eine Ausgabe erzeugen ([EVA-Prinzip](https://de.wikipedia.org/wiki/EVA-Prinzip) oder [IPO model](https://en.wikipedia.org/wiki/IPO_model)).

## Neue Konzepte

Das ursprüngliche Programm muss um drei Konzepte erweitert werden:

- Interaktion mit Benutzer / Eingabe
- Möglichkeit die Eingabe zu speichern
- Anpassung der Ausgabe unter Verwendung der Eingabe

### Variablen

Variablen stehen (vgl. zur Mathematik) als Platzhalter für Daten oder Werte die zum Zeitpunkt der Programmierung noch unbekannt sind. Sie sind ein Speicher für Daten und können gelesen oder geschrieben werden.

Jede Variable erhält einen eindeutigen Namen. Bei einer Zuweisung steht der Namen auf der linken Seite der Operation und auf der rechten Seitder Wert.

```python
my_name = "ralf"
```

Anschließend kann die Variable verwendet werden, um auf den gespeicherten
Wert zuzugreifen.

```python
print(my_name)
```

Intern hat jede Variable einen bestimmten Datentyp, der festlegt, welche Operationen zulässig sind. Beispielsweise führt die Verwendung Anführungszeichen dazu, dass die entstehenden Variable vom Typ **Zeichenkette** ist.

```python
my_name = "Ralf"  # my_name speichert eine Zeichenkette (Datentyp: String)
amount = 1000     # natürliche Zahl, kein Dezimalpunkt (Datentyp: integer)
dividend = 0.64   # reelle Zahl (Datentyp: float)
```

*Hinweis*: Für die Benennung der Variablen gibt es in Python Vorgaben, die die Lesbarkeit des Sourcecodes verbessern sollen. Siehe hierzu die [PEP8](https://peps.python.org/pep-0008/).

### Exkurs: Konstanten

In Python existieren auch Konstanten. Diese sind nicht veränderbar, sondern ihr Wert steht schon beim Programmieren fest und kann nur einmal zugewiesen werden. Sie können im weiteren Ablauf nur noch lesend verwendet werden. Das wird insbesondere dazu verwendet, den Sourcecode lesbarer zu gestalten.

```python
PI = 3.14159265358979323
WORK_URL = "https://www.th-owl.de"

```

*Hinweis*: Python erkennt Konstanten an der Großschreibung des Bezeichners.

### Ein- und Ausgabe von Daten

In [helloworld.py](../solutuions/helloworld.py) haben wir das Kommando `print()` zur Ausgabe von Text in die Konsole verwendet. In den Klammern hinter dem Kommando steht die Zeichenkette, die ausgegeben werden soll. Der Teil in den Klammern wird als **Aufrufparameter** bezeichnet.

Um eine Eingabe zu ermöglichen, verwenden wir die Funktion `input("NACHRICHT")`. Diese Kommando gibt die übergebene Nachricht in der Konsole aus und wartet anschließend darauf, dass der Benutzer eine Eingabe vornimmt. Die Eingabe wird durch das Drücken der ENTER-Taste abgeschlossen. Das Kommando gibt die Eingabe dann an das Programm zurück, so dass sie einer Variable zugewiesen werden kann. Die Rückgabe der Daten wird als **Rückgabewert bezeichnet**.

```python
# Der Benutzer soll seinen Name eingeben und wird dann persönlich begrüßt
your_name = input("Please enter your name:")
print("Hello " + your_name)
```

Bei Ausführung sieht das so aus:

![Erweitertes HelloWorld](../../_assets/01_start_ext_helloworld.png)

### Formatierung der Ausgabe

Im obigen Beispiel haben wir bei der Ausgabe einen konstanten Text und den Wert der Variable durch das +-Zeichen (exakt: durch **Konkatenation**) verknüpft. Sobald viele Daten gleichzeitig auszugeben sind, ist dieser Weg umständlich und fehleranfällig.
Stattdessen können wir innerhalb des Textes den Wert von Variablen einbetten, in dem wir den Namen der Variablen in geschweiften Klammern verwenden. Zusätzlich muss am Anfang der Zeichenkette der Operator *f* verwendet werden, diese Technik heißt daher auch **f-Strings**.

Untersuchen wir das folgende Beispiel:

```python
your_name = input("Please enter your name: ")
age = input("How old are you? ")

print(f"Welcome user {your_name}. Your current age is {age}.")
```

Zunächst wird Name und Alter abgefragt und in den beiden Variablen *your_name* und *age* gespeichert. Anschließend wird der Textblock `Welcome user {your_name}. Your current age is {age}` in die Konsole ausgegeben. Das vorangestellte *f* führt dazu, dass im Text die Variablennamen mit dem jeweiligen Wert ersetzt werden.

*Hinweis*: Probieren Sie aus, was ohne das vorangestellte *f* passiert.

>## 📣 Zusammenfassung
>
>- Variablen speichern Werte in Python-Programmen
>- Jede Variable hat einen eindeutigen Namen
>- Konstanten sind unveränderlich und können nur gelesen werden
>- Zur Ausgabe wird `print()`, für die Eingabe `input()` verwendet
>- Mit f-Strings kann die Ausgabe flexibel gestaltet werden. Sie erlauben das Einbetten von Variablen in Textblöcke
