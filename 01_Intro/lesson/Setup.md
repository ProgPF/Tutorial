---
topic: setup the development tool chain
summary: describes briefly, how to configure and install the needed tools.
tags: [toolchain, setup]
---
# Erste Schritte

## Benötigte Software

Für den Kurs werden benötigt:

- Aktuelle Installation des [Python SDK](https://www.python.org/downloads/), derzeit eine Version > 3.10
- Visual Studio Code mit folgenden Plugins:
  - [Python Extension Pack](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
- [Git](https://git-scm.com/downloads)

## Erster Start

Starten Sie Visual Studio durch Eingabe von `code` in der Kommandozeile oder klicken Sie das Icon in der Startleiste / auf dem Desktop an.

Überprüfen Sie, dass die benötigten Erweiterungen installiert sind:

![Python-Erweiterungen in Visual Studio Code](../../_assets/vsc_py_extensions.png)

## Verfügbarkeit Python-Installation

Überprüfen Sie die Installation, in dem Sie ein Konsolenfenster öffnen und das Kommando `python --version` eintippen (unter Linux müssen Sie `python3 --version` verwenden). Die Ausführung muss erfolgreich und die Versionsnummer > 3.10 sein:

![Ausgabe Python in Konsole](../../_assets/01_python_check_console.png)

## Dokumentation

Die Installation von Visual Studio Code und die Integration der Python-Tools ist auf Microsofts Dokumentationsseite gut beschrieben.
Verwenden Sie folgende Links, falls Sie mehr über die Einrichtung von Visual Studio Code erfahren wollen:

> **Downloads**
>
>- [Videoserie: Introduction to VS Code](https://code.visualstudio.com/docs/getstarted/introvideos)
>- [Getting Started with Python in VS Code](https://code.visualstudio.com/docs/python/python-tutorial)




[Weiter](MainWindow.md)