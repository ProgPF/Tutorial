# Hello World in Python

**Hello World**-Programme haben in der Informatik eine lange Tradition und dienen dazu, den ersten Kontakt mit einer Programmiersprache herzustellen. Sie sind sowas wie ein Proof-of-concept und zeigen, dass Sie ein minimales und funktionsfähiges Programm mit einer Programmiersprache **erstellen und ausführen** können.

Der **HelloWorld**-Ansatz geht auf C zurück:

![Hello World, Brian Kernighan, 1978](../../_assets/01_helloworld_in_c.jpg)

## HelloWorld in Python

1. Wechseln Sie in der Activity Bar auf die Ansicht **Explorer**
2. Erzeugen Sie eine neue Datei mit dem Namen `helloworld.py`. Achten Sie auf die Dateiendung **.py**. Alternativer Shortcut: `CTRL+ALT+SUPER+N`.
3. Geben Sie den Quelltext des Programms ein.
4. Starten Sie die Ausführung durch Betätigung von `F5`.

```python
# Das ist mein erstes Python-Programm
print("Hello world in Python")
```

Nach erfolgreicher Ausführung müssen Sie die Ausgabe **Hello World..** in der Konsole am unteren Bildschirmrand sehen:

![Ausführung Hello World](../../_assets/01_start_helloworld.png)

## Elemente des ersten Programmes

Im Source-Code von helloworld.py finden Sie zwei Elemente aus Python:

- `print("a message")`: Ein **Kommando**, das einen übergebenen Text in der Konsole ausgibt. Der auszugebende Wert wird in Anführungszeichen oder einem Apostroph eingeschlossen.
- `# Mein Programm` : Ein **Kommentar**, der die Lesbarkeit des Programms verbessert. Im Gegensatz zum Kommando wird der Kommentar kein Bestandteil des Programms.

### Kommandos

Kommandos führen eine bestimmte Teilfunktion des Programms aus. Beispielsweise kann das Kommando `print()` übergebene Werte in die Konsole ausgeben. Technisch handelt es sich bei allen Kommandos um **Funktionen**, die nicht nur Daten übernehmen sondern auch ein Ergebnis zurückgeben. Dies kann das Ergebnis einer Berechnung (wie bspw. in *sin(3.1415)* oder eine Statusmeldung wie **Erfolg** oder **Fehler** sein).
Die Kombination aus **Name der Funktion**, **Art und Anzahl der Aufrufparameter** und die **Art des Ergebnisses** wird als **Methodensignatur** bezeichnet. Visual Studio Code zeigt Ihnen während der Eingabe die Methodensignatur von Funktionen an:

![Methodensignatur print](../../_assets/01_syntax_help.png)

### Kommentare

Kommentare stellen Begleittext des eigentlichen Sourcecodes dar, in denen der Entwickler Abläufe und Entscheidungen erläutert. Sie sollen es anderen Entwicklern (bzw. sich selbst) ermöglichen, den Sourcecode besser zu verstehen.

Python kennt zwei Arten von Kommentaren:

- `#` - **Inline-Kommentar**:. Alle Zeichen hinter dem Hash-Zeichen werden als Kommentar betrachtet
- `"""` - Multiline-Kommentare: Größere Kommentarbereiche werden zwischen dem 3-fachen Anführungszeichen eingeschlossen. Die Kommentarsektion endet erst nach den schließenden Anführungszeichen.

```python
i = 5 # das ist eine Erläuterung, warum der Wert von i 5 ist.

""" 
Hier folgt ein längerer Kommentar über mehrere Zeilen. Er wird beispielsweise
verwendet, um die Aufgaben einer Funktion vorab zu beschreiben.
"""
```

Hinweis: Im [Python Style Guide (PEP8)](https://peps.python.org/pep-0008/) gibt es Vorgaben zur korrekten [Verwendung von Kommentaren](https://peps.python.org/pep-0008/#comments)

>## 📣 Zusammenfassung
>
>- Der Quelltext eines Python-Programms besteht aus Kommandos und aus Kommentaren
>- Jedes Kommando hat eine eindeutige Methodensignatur und führt eine bestimmte Funktion aus
>- Jedes Kommando ist eigentlich eine Funktion und liefert einen Wert zurück
>- Schwierige Code-Passagen werden durch Kommentare erläutert um ihre Verständlichkeit zu verbessern
>- Kommentare können ein- oder mehrzeilig sein


[Weiter](ExtHelloWorld.md)
