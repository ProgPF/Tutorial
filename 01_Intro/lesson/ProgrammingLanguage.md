# Programmiersprachen

Programmiersprachen werden in der Informatik verwendet, um die Lösung für ein Problem zu beschreiben. Hierfür sind natürliche Sprachen ungeeignet, da sie zahlreiche Mehrdeutigkeiten und eine viel zu großen Sprachumfang besitzen.
Ein Programm besteht daher aus einer Menge von Texten, die wir **Sourcecodes** nennen. Darin ist eine Lösungsvorschrift für das Gesamtproblem enthalten, die sich häufig in Module oder Bausteine gliedert. Auf der untersten Ebene der Gliederung finden sich dann Lösung für atomare Fragen; dies wird als [Algorithmus](https://de.wikipedia.org/wiki/Algorithmus) bezeichnet.


## Instruction Language

Ein Computerprogramm wird also in Form einer Anweisungsfolge beschrieben. Die Instruktionen, die erläutern wie die **Lösungsvorschrift** aussieht, können entweder:

- Zeilenorientierter Text, bspw. in C++, Python, Java
- Graphische Lösungsvorschrift, bspw. in Scratch oder Labview

erfasst werden. Darüber hinaus werden Programmiersprachen über ihren grundsätzlichen methodischen Aufbau unterschieden:

- **Prozedurale** Sprachen: die Grundeinheit ist eine Datenblock, der an Prozeduren und Funktionen übergeben wird. Beispiel: *C*
- **Objektorientierte** Sprachen: die Grundeinheit ist eine Kombination aus Daten und Verhalten, die als Objekt bezeichnet wird. Beispiel: *C++,Python, Java*
- **Funktionale** Sprachen: bestehen aus Funktionen, die andere Funktionen als Parameter übernehmen können. Mathematisches Konstrukt, in vielen Programmiersprachen als Lambda-Kalkül verankert. Beispiel: *F#*
- **Logische** Sprachen: Im Sourcecode wird statt einer Lösung das Problem als Fakten und Regeln (Axiomen) beschrieben. Das System leitet die Lösung selbst ab. Beispiel: *Prolog*

Die drei erstgenannten Klassen sind Vertreter der **imperativen** Programmiermodelle, die logischen Sprachen ordnen sich der **deklarativen** Programmierung unter.

## Syntax und Semantik

In der Software-Entwicklung (und in allen natürlichen Sprachen) wird zwischen der *Syntax* und der *Semantik* von Ausdrücken unterschieden. 

### Syntax

Die Syntax einer Sprache beschreibt die zulässigen Worte und ihre Verwendung über die Angabe einer Grammatik. Vergleichen Sie das mit dem deutschen Satz:

**Prokrammieren in. Bython is sher einfach?**

In diesem Satz sind zahlreiche grammatikalische und orthographische Fehler enthalten, die vom Syntax-Prüfer der Programmiersprache automatisch erkannt werden. Ihre Entwicklungsumgebung wird diese Fehler anzeigen. Man spricht hier von **statischer Prüfung** oder **Compile-Time-Checks**.

### Semantik 

Die Semantik beschreibt die Bedeutung eines Terms. Betrachten wir als Beispiel folgenden Satz:

**Programmieren in meinem Briefumschlag schmeckt sehr grün.**

Der Satz ist offensichtlich frei von syntaktischen Fehler, sein Inhalt ergibt aber keinen Sinn. In Programmen können solche semantischen Fehler enthalten sein, die später zur Fehlfunktion der ganzen Applikation führen.

Semantische Fehler kann ihre Entwicklungsumgebung nicht automatisch erkennen. Sie werden durch strukturiertes Testen aufgedeckt. Man spricht von **dynamischer Prüfung** oder **Run-Time-Checks**.


[Weiter](Setup.md)